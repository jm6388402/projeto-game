<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriasController extends Controller
{
    public function index(){
        return view('admin.categoria.index');
    }

    public function visualizar(){
        return view('admin.categoria.visualizar');
    }

    public function cadastrar(){
        return view('admin.categoria.cadastrar');
    }

    public function editar(){
        return view('admin.categoria.editar');
    }

    public function deletar(){
        echo 'Função Deletar';
    }
}
