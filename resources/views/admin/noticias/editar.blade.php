@extends('layouts.site')


@section('titulo','Área administrativa')
    
@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12 offset-4 text-danger">
            <h2>EDITAR NOTICIAS</h2>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
                @include('admin.noticias.form')
        </div>
    </div>
</div>

@endsection