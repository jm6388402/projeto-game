@extends('layouts.site')

@section('titulo','Área administrativa')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-12 offset-4 text-danger">
            <h2>VISUALIZAR CATEGORIAS</h2>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
           <table class="table table-striped table-condensed">
               <tr>
                    <th width="150">ID</th>
                    <td>1</td>
               </tr>
               <tr>
                    <th width="150">Título</th>
                    <td>Gran Turismo</td>
               </tr>
               <tr>
                    <th width="150">Subtitulo</th>
                    <td>Corrida</td>
               </tr>
               <tr>
                    <th width="150">Descrição</th>
                    <td>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur suscipit quaerat iste quae, quisquam porro esse dicta eius hic culpa, accusantium eaque ipsam neque ut consectetur obcaecati perspiciatis assumenda quibusdam.</td>
               </tr>
               <tr>
                    <th width="150">Status</th>
                    <td><strong>Não Publicado</strong></td>
               </tr>
           </table>
           <a href="{{ route('categoria-editar')}}" class="btn btn-primary">Editar Categoria</a>
        <a href="{{ route('categoria-index')}}" class="btn btn-danger">Cancelar</a>
        </div>
        
    </div>

</div>


@endsection