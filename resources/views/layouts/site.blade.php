<!DOCTYPE html>
<html lang="pt-br">

    <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ asset('css/site.css') }}">
        
    <title>Web Games - @yield('titulo')</title>

    </head>

    <body>
        <header>
            <div class="bgCinza bgzinza bct">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2">
                            <p>(14)4444-4444</p>
                        </div>
                        <div class="col-lg-3 offset-lg-6 icones">
                            <a href="#">
                                <i class=" fab fa-facebook-f"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#">
                                <i class="fab fa-google"></i>
                            </a>
                            <a href="#">
                                <i>Faça seu Login</i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bgCinza">
                <div class="container inline-block">
                    <div class="row">
                        <div class="col-lg-3">
                        <img src={{ asset('img/logo_webgames-semfundo-4.png')}}>
                        </div>
                        <div class="col-lg-9">

                            <div class="row col-meio buscar">
                                <div class="offset-2 col-lg-10 ">

                                    <div class="pesquisa">
                                        <form action="" method="get">
                                            <button class="fa fa-search"></button>
                                            <input type="text" name="pesquisa"
                                            placeholder="Pesquisa">
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="container">
                                    <nav id="menu">
                                        <ul>
                                            <li><a href="/">MENU</a></li>
                                            <li>
                                                <a href="anuncios">ANUNCIOS</a>
                                            </li>
                                            <li>
                                                <a href="eventos">EVENTOS</a>
                                            </li>
                                            <li>
                                                <a href="analises">ANALISES</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header>

        
        <section id="conteudo">
            @yield('conteudo')
        </section>


        
                <div id="rodape">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3">
                                                
                                    <a href="#">QUEM SOMOS</a><br>                
                                    <a href="#">NOSSA EQUIPE</a>
                                                
                                </div>
                                <div class="col-lg-3">  
                                    <a href="#">FALE CONOSCO</a><br>
                                    <a href="#">POLITICA E PRIVACIDADE</a>
                                </div>
                                
                                <div class="col-lg-3 log">
                                    <img src={{ asset('img/logo_webgames-semfundo-4.png')}}>
                                </div>
                            </div>
                            <div class="row tt">
                                
                                <p>COPYRIGHT &copy; 2018 WEBGAMES.COM.BR TODOS OS DIREITOS RESERVADOS.</p>
                            </div>
                        </div>
                    </div>
    </body>

</html>
