<?php

Route::get('/', function () {
    return view('home.home');
})->name('home');

Route::get('/anuncios', function () {
    return view('home.anuncios');
})->name('anuncios');

Route::get('/eventos', function () {
    return view('home.eventos');
})->name('eventos');
Route::get('/analises', function () {
    return view('home.analises');
})->name('analises');

Route::get('/admin/home', function () {
    return view('admin.home.index');
});

Route::prefix('admin')->namespace('Admin')->group(function () {

    Route::get('/noticias', 'NoticiasController@index')->name('noticias-index');

    Route::get('/noticias/cadastrar', 'NoticiasController@cadastrar')->name('noticias-cadastrar');

    Route::get('/noticias/editar', 'NoticiasController@editar')->name('noticias-editar');

    Route::get('/noticias/visualizar', 'NoticiasController@visualizar')->name('noticias-visualizar');

    Route::get('/noticias/deletar', 'NoticiasController@deletar')->name('noticias-deletar');

    ///////////////////////////////////////////////////////////////////////////////////////

    Route::get('/categoria', 'CategoriasController@index')->name('categoria-index');

    Route::get('/categoria/cadastrar', 'CategoriasController@cadastrar')->name('categoria-cadastrar');

    Route::get('/categoria/editar', 'CategoriasController@editar')->name('categoria-editar');

    Route::get('/categoria/visualizar', 'CategoriasController@visualizar')->name('categoria-visualizar');

    Route::get('/categoria/deletar', 'CategoriasController@deletar')->name('categoria-deletar');

    Route::get('/categoria', 'CategoriasController@index')->name('categoria-index');

    ///////////////////////////////////////////////////////////////////////////////////////

    Route::get('/usuarios', 'UsuariosController@index')->name('usuarios-index');

    Route::get('/usuarios/cadastrar', 'UsuariosController@cadastrar')->name('usuarios-cadastrar');

    Route::get('/usuarios/editar', 'UsuariosController@editar')->name('usuarios-editar');

    Route::get('/usuarios/visualizar', 'UsuariosController@visualizar')->name('usuarios-visualizar');

    Route::get('/usuarios/deletar', 'UsuariosController@deletar')->name('usuarios-deletar');

});
    

